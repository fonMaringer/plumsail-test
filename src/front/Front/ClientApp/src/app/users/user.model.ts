﻿export class User{

  constructor(){
    this.DataType = "User";
  }

  DataType: string;
  name: string;
  birthday: Date;
  phone: string;
  email: string;
  sex: Sex;
  creditCard: string;
}
