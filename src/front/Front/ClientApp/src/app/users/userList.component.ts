﻿import { Component} from '@angular/core';
import { HttpService} from '../common/http.service';
import {User} from './user.model';

@Component({
  selector: 'app-home',
  templateUrl: './userList.component.html',
  providers: [HttpService]
})
export class UserListComponent{
  users: User[];

  constructor(private httpService: HttpService){
    this.httpService.getData("User")
      .subscribe(
        (data: User[]) => {this.users=data;},
        error => console.log(error)
      );
  }
}
