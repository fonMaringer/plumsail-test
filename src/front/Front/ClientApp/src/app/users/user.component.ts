﻿import { Component} from '@angular/core';
import { Router} from '@angular/router';
import { HttpService} from '../common/http.service';
import {User} from './user.model';

@Component({
  selector: 'app-home',
  templateUrl: './user.component.html',
  providers: [HttpService]
})
export class UserComponent{
  user: User = new User();

  constructor(private httpService: HttpService, private router: Router){}
  submit(user: User){
    this.httpService.addData(user)
      .subscribe(
        () => {this.router.navigate(['/users'])},
        error => console.log(error)
      );
  }
}
