﻿import { Component} from '@angular/core';
import { Router} from '@angular/router';
import { HttpService} from '../common/http.service';
import {Order} from './order.model';

@Component({
  selector: 'app-home',
  templateUrl: './order.component.html',
  providers: [HttpService]
})
export class OrderComponent{
  order: Order = new Order();

  constructor(private httpService: HttpService, private router: Router){}
  submit(order: Order){
    this.httpService.addData(order)
      .subscribe(
        () => {this.router.navigate(['/orders'])},
        error => console.log(error)
      );
  }
}
