﻿import { Component} from '@angular/core';
import { HttpService} from '../common/http.service';
import {Order} from "./order.model";

@Component({
  selector: 'app-home',
  templateUrl: './orderList.component.html',
  providers: [HttpService]
})
export class OrderListComponent{
  orders: Order[];

  constructor(private httpService: HttpService){
    this.httpService.getData("Order")
      .subscribe(
        (data: Order[]) => {this.orders=data;},
        error => console.log(error)
      );
  }
}
