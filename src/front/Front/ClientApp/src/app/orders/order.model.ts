﻿export class Order{

  constructor(){
    this.DataType = "Order";
  }

  DataType: string;
  date: Date;
  orderId: string;
  summary: number;
  shipping: Shipping;
  isCompleted: boolean;
}
