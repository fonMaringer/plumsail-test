﻿import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from "../../environments/environment";

@Injectable()
export class HttpService{
  constructor(private http: HttpClient){ }
  addData(data: object){
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(environment.backAddress + "Storage/Add", data, { headers: headers });
  }
  getData(dataType: string){
    return this.http.get(environment.backAddress + "Storage/Get", {params: {"dataType" : dataType}});
  }
}
