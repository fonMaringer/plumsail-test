import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { OrderComponent } from './orders/order.component';
import { OrderListComponent } from './orders/orderList.component';
import { UserComponent } from './users/user.component';
import { UserListComponent } from './users/userList.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    OrderComponent,
    OrderListComponent,
    UserComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'createOrder', component: OrderComponent, pathMatch: 'full' },
      { path: 'orders', component: OrderListComponent, pathMatch: 'full' },
      { path: 'createUser', component: UserComponent, pathMatch: 'full' },
      { path: 'users', component: UserListComponent, pathMatch: 'full' }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
