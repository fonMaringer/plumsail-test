﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dal.Infrastructure;
using Domain.Interfaces;

namespace Dal.Classes
{
    internal sealed class Repository<TEntity, TFilter> : IRepository<TEntity, TFilter> 
        where TEntity : class, IId
        where TFilter : class, IFilter<TEntity>
    {
        #region fields

        private readonly IDbContext _dbContext;

        #endregion

        #region constructor

        public Repository(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #endregion

        #region properties



        #endregion

        #region public methods

        public void Add(TEntity entity)
        {
            _dbContext.Insert(entity);
        }

        public Task AddAsync(TEntity entity)
        {
            return _dbContext.InsertAsync(entity);
        }

        public void Delete(TEntity entity)
        {
            _dbContext.Delete<TEntity>(x => x.Id == entity.Id);
        }

        public Task DeleteAsync(TEntity entity)
        {
            return _dbContext.DeleteAsync<TEntity>(x => x.Id == entity.Id);
        }

        public void DeleteAll(TFilter filter = null)
        {
            _dbContext.DeleteAll(filter?.Expression);
        }

        public Task DeleteAllAsync(TFilter filter = null)
        {
            return _dbContext.DeleteAllAsync(filter?.Expression);
        }

        public TEntity Get(TFilter filter)
        {
            var res = _dbContext.GetSingle(filter?.Expression);
            return res;
        }

        public Task<TEntity> GetAsync(TFilter filter)
        {
            var res = _dbContext.GetSingleAsync(filter?.Expression);
            return res;
        }

        public IEnumerable<TEntity> GetList()
        {
            var res = _dbContext.GetList<TEntity>();
            return res;
        }

        public Task<IEnumerable<TEntity>> GetListAsync()
        {
            var res = _dbContext.GetListAsync<TEntity>();
            return res;
        }

        public IEnumerable<TEntity> GetList(TFilter filter)
        {
            var res = _dbContext.GetList(filter?.Expression);
            return res;
        }

        public Task<IEnumerable<TEntity>> GetListAsync(TFilter filter)
        {
            var res = _dbContext.GetListAsync(filter?.Expression);
            return res;
        }

        public void Update(TEntity entity)
        {
            _dbContext.Update(x => x.Id == entity.Id, entity);
        }

        public Task UpdateAsync(TEntity entity)
        {
            return _dbContext.UpdateAsync(x => x.Id == entity.Id, entity);
        }
        
        #endregion

        #region protected methods



        #endregion

        #region private methods



        #endregion
    }
}