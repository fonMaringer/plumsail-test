﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Dal.Infrastructure;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace Dal.Classes
{
    internal sealed class DbContext : IDbContext
    {
        #region fields

        private const string ConfigDbPath = "Database:FullPath";

        private readonly IMongoDatabase _database;

        #endregion

        #region constructor

        public DbContext(IConfiguration config)
        { 
            var connection = new MongoUrlBuilder(config[ConfigDbPath]);
            var client = new MongoClient(config[ConfigDbPath]);
            _database = client.GetDatabase(connection.DatabaseName);
        }

        #endregion

        #region properties



        #endregion

        #region public methods

        public void Insert<T>(T entity)
        {
            var collection = DbCollection<T>();
            collection.InsertOne(entity);
        }

        public async Task InsertAsync<T>(T entity)
        {
            await DbCollection<T>().InsertOneAsync(entity);
        }

        public void Update<T>(Expression<Func<T, bool>> filter, T entity)
        {
            DbCollection<T>().ReplaceOne(filter, entity);
        }

        public async Task UpdateAsync<T>(Expression<Func<T, bool>> filter, T entity)
        {
            await DbCollection<T>().ReplaceOneAsync(filter, entity);
        }

        public void Delete<T>(Expression<Func<T, bool>> filter)
        {
            DbCollection<T>().DeleteOne(filter);
        }

        public async Task DeleteAsync<T>(Expression<Func<T, bool>> filter)
        {
            await DbCollection<T>().DeleteOneAsync(filter);
        }

        public void DeleteAll<T>(Expression<Func<T, bool>> filter = null)
        {
            if (filter == null)
                DbCollection<T>().DeleteMany(_ => true);
            else
                DbCollection<T>().DeleteMany(filter);
        }

        public async Task DeleteAllAsync<T>(Expression<Func<T, bool>> filter = null)
        {
            if (filter == null)
                await DbCollection<T>().DeleteManyAsync(_ => true);
            else
                await DbCollection<T>().DeleteManyAsync(filter);
        }

        public T GetSingle<T>(Expression<Func<T, bool>> filter)
        {
            return Find(filter).FirstOrDefault();
        }

        public async Task<T> GetSingleAsync<T>(Expression<Func<T, bool>> filter)
        {
            return await Find(filter).FirstOrDefaultAsync();
        }

        public IEnumerable<T> GetList<T>(Expression<Func<T, bool>> filter = null)
        {
            return Find(filter).ToList();
        }

        public async Task<IEnumerable<T>> GetListAsync<T>(Expression<Func<T, bool>> filter = null)
        {
            return await Find(filter).ToListAsync();
        }

        #endregion

        #region protected methods



        #endregion

        #region private methods

        private IMongoCollection<T> DbCollection<T>()
        {
            return _database.GetCollection<T>(typeof(T).Name); 
        }

        private IFindFluent<T, T> Find<T>(Expression<Func<T, bool>> filter)
        {
            return filter == null
                ? DbCollection<T>().Find(GetAll<T>())
                : DbCollection<T>().Find(filter);
        }
        
        private FilterDefinition<T> GetAll<T>()
        {
            var builder = new FilterDefinitionBuilder<T>();
            var filter = builder.Empty; 
            return filter;
        }
        
        #endregion
    }
}