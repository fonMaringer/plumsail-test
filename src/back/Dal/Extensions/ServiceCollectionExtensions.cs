﻿using Dal.Classes;
using Dal.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace Dal.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDal(this IServiceCollection services)
        {
            services.AddTransient<IDbContext, DbContext>();
            services.AddTransient(typeof(IRepository<,>), typeof(Repository<,>));
        }
    }
}