﻿using System;
using System.Linq.Expressions;

namespace Domain.Interfaces
{
    public interface IFilter<T>
    {
        Expression<Func<T, bool>> Expression { get; }
    }
}