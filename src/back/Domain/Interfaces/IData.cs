﻿using System.Collections.Generic;

namespace Domain.Interfaces
{
    public interface IData
    {
        Dictionary<string, object> Data { get; }
    }
}