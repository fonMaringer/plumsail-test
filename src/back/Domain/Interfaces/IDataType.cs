﻿namespace Domain.Interfaces
{
    public interface IDataType
    {
        string DataType { get; set; }
    }
}