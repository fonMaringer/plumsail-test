﻿using System;

namespace Domain.Interfaces
{
    public interface IId
    {
        Guid Id { get; }
    }
}