﻿using System;
using System.Collections.Generic;
using Domain.Interfaces;
using MongoDB.Bson.Serialization.Attributes;

namespace Domain.Model
{
    [BsonIgnoreExtraElements]
    public sealed class DataModel : IId, IDataType, IData
    {
        #region fields



        #endregion

        #region constructor

        public DataModel()
        {
            Id = Guid.NewGuid();
            Data = new Dictionary<string, object>();
        }

        #endregion

        #region properties

        [BsonId]
        public Guid Id { get; set; }
        
        public string DataType { get; set; }
        
        public Dictionary<string, object> Data { get; set; }

        #endregion

        #region public methods



        #endregion

        #region protected methods



        #endregion

        #region private methods



        #endregion
    }
}