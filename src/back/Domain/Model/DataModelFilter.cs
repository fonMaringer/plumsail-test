﻿using System;
using System.Linq.Expressions;
using Domain.Interfaces;

namespace Domain.Model
{
    public sealed class DataModelFilter : IFilter<DataModel>
    {
        #region fields



        #endregion

        #region constructor

        public DataModelFilter(string dataType)
        {
            Expression = m => m.DataType == dataType;
        }

        #endregion

        #region properties

        public Expression<Func<DataModel, bool>> Expression { get; }

        #endregion

        #region public methods



        #endregion

        #region protected methods



        #endregion

        #region private methods



        #endregion
    }
}