﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Dal.Infrastructure
{
    public interface IDbContext
    {
        void Insert<T>(T entity);

        Task InsertAsync<T>(T entity);

        void Update<T>(Expression<Func<T, bool>> filter, T entity);

        Task UpdateAsync<T>(Expression<Func<T, bool>> filter, T entity);

        void Delete<T>(Expression<Func<T, bool>> filter);

        Task DeleteAsync<T>(Expression<Func<T, bool>> filter);

        void DeleteAll<T>(Expression<Func<T, bool>> filter = null);

        Task DeleteAllAsync<T>(Expression<Func<T, bool>> filter = null);

        T GetSingle<T>(Expression<Func<T, bool>> filter);

        Task<T> GetSingleAsync<T>(Expression<Func<T, bool>> filter);

        IEnumerable<T> GetList<T>(Expression<Func<T, bool>> filter = null);

        Task<IEnumerable<T>> GetListAsync<T>(Expression<Func<T, bool>> filter = null);
    }
}