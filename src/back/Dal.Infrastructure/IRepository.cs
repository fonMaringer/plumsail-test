﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Interfaces;

namespace Dal.Infrastructure
{
    public interface IRepository<TEntity, in TFilter>
        where TEntity: class
        where TFilter: class, IFilter<TEntity>
    {
        void Add(TEntity entity);
        Task AddAsync(TEntity entity);
        void Update(TEntity entity);
        Task UpdateAsync(TEntity entity);
        void Delete(TEntity entity);
        Task DeleteAsync(TEntity entity);
        void DeleteAll(TFilter filter = null);
        Task DeleteAllAsync(TFilter filter = null);
        TEntity Get(TFilter filter);
        Task<TEntity> GetAsync(TFilter filter);
        IEnumerable<TEntity> GetList();
        Task<IEnumerable<TEntity>> GetListAsync();
        IEnumerable<TEntity> GetList(TFilter filter);
        Task<IEnumerable<TEntity>> GetListAsync(TFilter filter);
    }
}