﻿using System;
using System.Threading.Tasks;
using Domain.Interfaces;
using Domain.Model;
using Infrastructure.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Storage.Controllers
{
    public class StorageController : Controller
    {
        #region fields

        private readonly IDataProvider _dataProvider;
        private readonly IDtoConverter _dtoConverter;
        private readonly ILogger<StorageController> _logger;

        #endregion

        #region constructors

        public StorageController(IDataProvider dataProvider, IDtoConverter dtoConverter, ILogger<StorageController> logger)
        {
            _dataProvider = dataProvider;
            _dtoConverter = dtoConverter;
            _logger = logger;
        }

        #endregion

        #region public methods

        [HttpGet]
        public async Task<IActionResult> Get(string dataType)
        {
            var models = await _dataProvider.GetDataAsync(dataType);

            var dto = _dtoConverter.ConvertToDto(models);
            
            return Json(dto, new JsonSerializerSettings{Formatting = Formatting.None});
        }
        
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] JObject formFields)
        {
            if (!formFields.ContainsKey(nameof(IDataType.DataType)))
                return BadRequest($"No required field named {nameof(IDataType.DataType)}!");

            try
            {
                var model = _dtoConverter.ConvertFromDto<DataModel>(formFields);
                await _dataProvider.AddDataAsync(model);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                return StatusCode(500);
            }
            
            return Ok();
        }

        #endregion
    }
}