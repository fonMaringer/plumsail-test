﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Model;

namespace Infrastructure.Interfaces
{
    public interface IDataProvider
    {
        Task AddDataAsync(DataModel data);

        Task<IEnumerable<DataModel>> GetDataAsync(string dataType);
    }
}