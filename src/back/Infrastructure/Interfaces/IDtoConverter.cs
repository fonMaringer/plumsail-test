﻿using System.Collections.Generic;
using Domain.Interfaces;
using Newtonsoft.Json.Linq;

namespace Infrastructure.Interfaces
{
    public interface IDtoConverter
    {
        JObject ConvertToDto<T>(T model) where T : class, IDataType, IData;
        JArray ConvertToDto<T>(IEnumerable<T> models) where T : class, IDataType, IData;
        T ConvertFromDto<T>(JObject data) where T : class, IDataType, IData, new();
    }
}