﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Domain.Interfaces;
using Infrastructure.Interfaces;
using Newtonsoft.Json.Linq;

namespace Infrastructure.Classes
{
    internal sealed class DtoConverter : IDtoConverter
    {
        #region fields



        #endregion

        #region constructor

        

        #endregion

        #region properties



        #endregion

        #region public methods

        public JObject ConvertToDto<T>(T model) where T : class, IDataType, IData
        {
            if (model == null)
                return null;
            
            var dto = new JObject
            {
                [nameof(IDataType.DataType)] = model.DataType
            };

            foreach (var (key, o) in model.Data)
            {
                var value = TransformStoredData(o);
                dto[key] = value;
            }

            return dto;
        }

        public JArray ConvertToDto<T>(IEnumerable<T> models) where T : class, IDataType, IData
        {
            if (models == null)
                return null;
            
            var dto = new JArray();

            foreach (var model in models)
            {
                var modelDto = ConvertToDto(model);
                dto.Add(modelDto);
            }

            return dto;
        }

        public T ConvertFromDto<T>(JObject data) where T : class, IDataType, IData, new()
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            
            if (!data.TryGetValue(nameof(IDataType.DataType), out var dataType))
                throw new ConstraintException(nameof(IDataType.DataType));
            
            var model = new T
            {
                DataType = dataType.Value<string>(),
            };

            foreach (var (key, token) in data)
            {
                if (key == nameof(IDataType.DataType)) continue;

                switch (token)
                {
                    case JArray array:
                        model.Data[key] = array.Select(v => v.Value<string>()).ToList();
                        break;
                    case JValue value:
                        model.Data[key] = value.Value;
                        break;
                }
            }

            return model;
        }

        #endregion

        #region protected methods



        #endregion

        #region private methods

        private JToken TransformStoredData(object data)
        {
            switch (data)
            {
                case List<string> strings:
                    return new JArray(strings);
            }

            return new JValue(data);
        }

        #endregion
    }
}