﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dal.Infrastructure;
using Domain.Model;
using Infrastructure.Interfaces;

namespace Infrastructure.Classes
{
    internal sealed class DataProvider : IDataProvider
    {
        #region fields

        private readonly IRepository<DataModel, DataModelFilter> _repository;

        #endregion

        #region constructor

        public DataProvider(IRepository<DataModel, DataModelFilter> repository)
        {
            _repository = repository;
        }

        #endregion

        #region properties



        #endregion

        #region public methods

        public Task AddDataAsync(DataModel data)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            
            if (string.IsNullOrWhiteSpace(data.DataType))
                throw new ConstraintException();
            
            return _repository.AddAsync(data);
        }

        public Task<IEnumerable<DataModel>> GetDataAsync(string dataType)
        {
            if (string.IsNullOrWhiteSpace(dataType))
                throw new ArgumentException(nameof(dataType));

            return _repository.GetListAsync(new DataModelFilter(dataType));
        }

        #endregion

        #region protected methods



        #endregion

        #region private methods



        #endregion
    }
}