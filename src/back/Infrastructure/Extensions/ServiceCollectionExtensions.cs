﻿using Infrastructure.Classes;
using Infrastructure.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddInfrastructure(this IServiceCollection services)
        {
            services.AddSingleton<IDataProvider, DataProvider>();
            services.AddSingleton<IDtoConverter, DtoConverter>();
        }
    }
}