#!/bin/bash

docker-compose stop
docker rm &(docker ps -aq -f "name=plumsail_service")
docker rmi $(docker images -q -f dangling=true)
docker build -t plumsail_service .
docker-compose up -d
