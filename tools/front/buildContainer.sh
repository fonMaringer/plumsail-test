#!/bin/bash

docker-compose stop
docker rm &(docker ps -aq -f "name=plumsail_front")
docker rmi $(docker images -q -f dangling=true)
docker build -t plumsail_front .
docker-compose up -d
