﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dal.Infrastructure;
using Domain.Model;
using Infrastructure.Interfaces;
using Moq;
using NUnit.Framework;

namespace Infrastructure.Tests.DataProvider
{
    [TestFixture]
    public class AddTests
    {
        private DataModel _addedModel;
        private IDataProvider _dataProvider;
        
        [SetUp]
        public void Setup()
        {
            var repositoryMock = new Mock<IRepository<DataModel, DataModelFilter>>();
            repositoryMock.Setup(r => r.AddAsync(It.IsAny<DataModel>())).Returns((DataModel m) =>
            {
                _addedModel = m;
                return Task.FromResult(0);
            });

            _dataProvider = new Classes.DataProvider(repositoryMock.Object);
        }

        [TearDown]
        public void Teardown()
        {
            _addedModel = null;
        }

        [Test]
        public async Task FullData_Success()
        {
            var model = new DataModel
            {
                DataType = "order",
                Data = new Dictionary<string, object>
                {
                    ["field1"] = "value1",
                    ["field2"] = "value2",
                },
            };

            await _dataProvider.AddDataAsync(model);
            
            Assert.AreSame(_addedModel, model);
        }

        [Test]
        public async Task Add_EmptyData_Success()
        {
            var model = new DataModel
            {
                DataType = "order",
            };

            await _dataProvider.AddDataAsync(model);
            
            Assert.AreSame(_addedModel, model);
        }

        [Test]
        public void Add_NoDataType_Failed()
        {
            var model = new DataModel
            {
                Data = new Dictionary<string, object>
                {
                    ["field1"] = "value1",
                    ["field2"] = "value2",
                },
            };

            Assert.ThrowsAsync<ConstraintException>(async () => await _dataProvider.AddDataAsync(model));
            Assert.IsNull(_addedModel);
        }

        [Test]
        public void Add_Null_Failed()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await _dataProvider.AddDataAsync(null));
            Assert.IsNull(_addedModel);
        }
    }
}