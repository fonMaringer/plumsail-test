﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dal.Infrastructure;
using Domain.Model;
using Infrastructure.Interfaces;
using Moq;
using NUnit.Framework;

namespace Infrastructure.Tests.DataProvider
{
    [TestFixture]
    public class GetTests
    {
        private List<DataModel> _storedData;
        private IDataProvider _dataProvider;
        
        [SetUp]
        public void Setup()
        {
            _storedData = new List<DataModel>
            {
                new DataModel(),
                new DataModel(),
                new DataModel(),
            };
            
            var repositoryMock = new Mock<IRepository<DataModel, DataModelFilter>>();
            repositoryMock.Setup(r => r.GetListAsync(It.IsAny<DataModelFilter>())).Returns((DataModelFilter f) =>
                Task.FromResult((IEnumerable<DataModel>) _storedData));

            _dataProvider = new Classes.DataProvider(repositoryMock.Object);
        }

        [Test]
        public async Task Get_Normal_Success()
        {
            var result = await _dataProvider.GetDataAsync("order");
            
            Assert.AreEqual(_storedData.Count, result.Count());
        }

        [Test]
        public void Get_NoDataType_Failed()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await _dataProvider.GetDataAsync(null));
        }
    }
}