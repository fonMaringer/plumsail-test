﻿using System;
using System.Collections.Generic;
using System.Data;
using Domain.Model;
using Infrastructure.Interfaces;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Infrastructure.Tests.DtoConverter
{
    [TestFixture]
    public class ConvertFromDtoTests
    {
        private IDtoConverter _converter;
        
        [SetUp]
        public void Setup()
        {
            _converter = new Classes.DtoConverter();
        }

        [Test]
        public void ConvertFromDto_FullData_Success()
        {
            var data = new JObject
            {
                ["DataType"] = "order",
                ["Field1"] = "value1",
                ["Field2"] = new JArray{"set", "of", "values"},
                ["Field3"] = null,
            };

            var model = _converter.ConvertFromDto<DataModel>(data);
            
            Assert.AreEqual("order", model.DataType);
            Assert.AreEqual(3, model.Data.Count);
            Assert.AreEqual("value1", model.Data["Field1"]);
            Assert.IsInstanceOf<List<string>>(model.Data["Field2"]);
            Assert.AreEqual(3, ((List<string>) model.Data["Field2"]).Count);
            Assert.IsNull(model.Data["Field3"]);
        }

        [Test]
        public void ConvertFromDto_EmptyData_Success()
        {
            var data = new JObject
            {
                ["DataType"] = "order",
            };

            var model = _converter.ConvertFromDto<DataModel>(data);
            
            Assert.AreEqual("order", model.DataType);
            Assert.AreEqual(0, model.Data.Count);
        }

        [Test]
        public void ConvertFromDto_NoDataType_Failed()
        {
            var data = new JObject
            {
                ["Field1"] = "value1",
                ["Field2"] = new JArray{"set", "of", "values"},
            };

            Assert.Throws<ConstraintException>(() => _converter.ConvertFromDto<DataModel>(data));
        }

        [Test]
        public void ConvertFromDto_NullDto_Failed()
        {
            Assert.Throws<ArgumentNullException>(() => _converter.ConvertFromDto<DataModel>(null));
        }
    }
}