﻿using System.Collections.Generic;
using Domain.Interfaces;
using Domain.Model;
using Infrastructure.Interfaces;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Infrastructure.Tests.DtoConverter
{
    [TestFixture]
    public class ConvertToDtoTests
    {
        private IDtoConverter _converter;
        
        [SetUp]
        public void Setup()
        {
            _converter = new Classes.DtoConverter();
        }

        [Test]
        public void ConvertToDto_Single_FullData_Success()
        {
            var model = new DataModel
            {
                DataType = "order",
                Data = new Dictionary<string, object>
                {
                    ["Field1"] = "value1",
                    ["Field2"] = new List<string>{"set", "of", "values"},
                    ["Field3"] = null,
                },
            };

            var dto = _converter.ConvertToDto(model);
            
            Assert.AreEqual(4, dto.Count);
            Assert.AreEqual("order", dto[nameof(IDataType.DataType)].Value<string>());
            Assert.AreEqual("value1", dto["Field1"].Value<string>());
            Assert.IsInstanceOf<JArray>(dto["Field2"]);
            Assert.AreEqual(3, ((JArray)dto["Field2"]).Count);
            Assert.IsEmpty(dto["Field3"]);
        }

        [Test]
        public void ConvertToDto_Single_EmptyData_Success()
        {
            var model = new DataModel();

            var dto = _converter.ConvertToDto(model);
            
            Assert.AreEqual(1, dto.Count);
            Assert.IsEmpty(dto[nameof(IDataType.DataType)]);
        }

        [Test]
        public void ConvertToDto_Multiple_FullData_Success()
        {
            var model1 = new DataModel
            {
                DataType = "order",
                Data = new Dictionary<string, object>
                {
                    ["Field1"] = "value1",
                    ["Field2"] = new List<string>{"set", "of", "values"},
                    ["Field3"] = null,
                },
            };
            var model2 = new DataModel
            {
                DataType = "order",
                Data = new Dictionary<string, object>
                {
                    ["Field1"] = "value1",
                    ["Field2"] = new List<string>{"set", "of", "values"},
                    ["Field3"] = null,
                },
            };

            var models = new List<DataModel>
            {
                model1, 
                model2
            };

            var dto = _converter.ConvertToDto(models);
            
            Assert.IsInstanceOf<JArray>(dto);

            Assert.AreEqual(2, dto.Count);
        }

        [Test]
        public void ConvertToDto_Single_NullData_Success()
        {
            var dto = _converter.ConvertToDto((DataModel) null);
            
            Assert.IsNull(dto);
        }

        [Test]
        public void ConvertToDto_Multiple_NullData_Success()
        {
            var dto = _converter.ConvertToDto((IEnumerable<DataModel>) null);
            
            Assert.IsNull(dto);
        }
    }
}